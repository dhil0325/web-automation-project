package app.binar.pages.demoblaze;

import app.binar.handler.Action;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.time.Duration;

public class ContactPage {

    WebDriver webDriver;

    Action action;

    // Constructor : akan di jalankan pertama kali
    public ContactPage(WebDriver driver) {
        this.webDriver = driver; // local class webdriver
        this.action = new Action(this.webDriver);
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(id = "recipient-email")
    private WebElement contactEmailField;

    @FindBy(id = "recipient-name")
    private WebElement contactNameField;

    @FindBy(id = "message-text")
    private WebElement messageField;

    @FindBy(xpath = "//*[@id='exampleModal']/div/div/div[3]/button[2]")
    private WebElement btnSend;

    @FindBy(xpath = "//*[@id='exampleModal']/div/div/div[3]/button[1]")
    private WebElement btnClose;

    @FindBy(xpath = "//*[@id='exampleModal']/div/div/div[1]/button/span")
    private WebElement btnX;

    public void enterFieldContact(String contactEmail, String contactName, String message){
        enterContactEmail(contactEmail);
        enterContactName(contactName);
        enterMessage(message);
        tapButtonSend();
    }

    public void enterContactEmail(String contactEmail){
        action.waitElementToBeDisplayed(contactEmailField, 3);
        contactEmailField.sendKeys(contactEmail);
    }

    public void enterContactName(String contactName){
        action.waitElementToBeDisplayed(contactNameField, 3);
        contactNameField.sendKeys(contactName);
    }

    public void enterMessage(String message){
        action.waitElementToBeDisplayed(messageField,3);
        messageField.sendKeys(message);
    }

    public void tapButtonSend(){
        action.waitElementToBeDisplayed(messageField, 3);
        btnSend.click();
    }






}
