package app.binar.pages.demoblaze;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class LoginPage {

    WebDriver webDriver;
    public LoginPage(WebDriver driver) {
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "loginusername")
    private WebElement userNameField;

    @FindBy(id = "loginpassword")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id='logInModal']/div/div/div[3]/button[2]")
    private WebElement btnLogin;

    @FindBy(xpath = "//*[@id='logInModal']/div/div/div[3]/button[1]")
    private WebElement btnClose;

    @FindBy(xpath = "//*[@id='logInModal']/div/div/div[1]/button")
    private WebElement btnX;


    public void enterCredential(String userName, String password){
        userNameField.sendKeys(userName);
        passwordField.sendKeys(password);
    }


    public void tapLogin(){
        btnLogin.click();
    }
    public void tapClose(){
        btnClose.click();
    }

    public void tapX(){
        btnX.click();
    }


}
