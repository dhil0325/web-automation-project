package app.binar.pages.demoblaze;

import app.binar.handler.Action;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class HomePage {

    WebDriver webDriver;


    public HomePage(WebDriver driver) {
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "login2")
    private WebElement navLinkLogin;

    @FindBy(id = "signin2")
    private WebElement navLinkSignUp;
    @FindBy(id = "nameofuser")
    private WebElement navLinkNameOfUser;

    @FindBy(xpath = "//*[@id='navbarExample']/ul/li[2]/a")
    private WebElement navLinkContact;

    public void tapNavLinkLogin(){

        navLinkLogin.click();
    }

    public void tapNavLinkSignUp() {
        navLinkSignUp.click();
    }

    public void tapNavLinkContact() {
        navLinkContact.click();
    }

    public String getNameOfUser() {
        Action action = new Action(webDriver);
        action.validateElementIsVisibleAndEnabled(navLinkNameOfUser);
        return navLinkNameOfUser.getText();
    }


}
