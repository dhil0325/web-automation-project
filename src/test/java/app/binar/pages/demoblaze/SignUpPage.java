package app.binar.pages.demoblaze;

import app.binar.handler.Action;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class SignUpPage {
    WebDriver webDriver;

    public SignUpPage(WebDriver driver) {
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(id = "sign-username")
    private WebElement userNameField;

    @FindBy(id = "sign-password")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id='signInModal']/div/div/div[3]/button[2]")
    private WebElement btnSignUp;

    @FindBy(xpath = "//*[@id='signInModal']/div/div/div[3]/button[1]")
    private WebElement btnClose;

    @FindBy(xpath = "//*[@id='signInModal']/div/div/div[1]/button")
    private WebElement btnX;

    public void enterSignup(String UserName, String password) {
        userNameField.sendKeys(UserName);
        passwordField.sendKeys(password);
    }
    public void tapSignUp() { btnSignUp.click(); }
    public void tapClose() { btnClose.click(); }
    public void tapX() { btnX.click(); }



    }



