package app.binar.pages.midtrans;

import app.binar.pages.demoblaze.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class Homepage {

    WebDriver webDriver;

    public Homepage(WebDriver driver) {
        this.webDriver = driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }
    @FindBy(xpath = "//*[@id='container']/div/div/div[1]/div[2]/div/div/a")
    private WebElement btnBuy;

    public void tapBtnBuy(){
    btnBuy.click();
}

}
