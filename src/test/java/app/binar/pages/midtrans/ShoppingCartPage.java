package app.binar.pages.midtrans;

import app.binar.handler.Action;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class ShoppingCartPage {
    protected WebDriver webDriver;
    protected Action action;

    public ShoppingCartPage(WebDriver driver) {
        this.webDriver = driver;
        this.action = new Action(this.webDriver);
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        PageFactory.initElements(webDriver, this);
    }

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[3]/input")
    private WebElement fieldAmount;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[1]/td[2]/input")
    private WebElement fieldName;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[2]/td[2]/input")
    private WebElement fieldEmail;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[1]/td[2]/input")
    private WebElement fieldPhoneNo;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[4]/td[2]/input")
    private WebElement fieldCity;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[5]/td[2]/textarea")
    private WebElement fieldAddress;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[1]/div[4]/table/tbody/tr[6]/td[2]/input")
    private WebElement fieldPostal;

    @FindBy (xpath = "//*[@id='container']/div/div/div[2]/div[2]/div[1]")
    private WebElement btnCheckout;

    public void enterAmount(String amount) {
        action.waitElementToBeDisplayed(fieldAmount, 3);
        fieldAmount.sendKeys(amount);
    }

    public void enterName(String name) {
        action.waitElementToBeDisplayed(fieldName, 3);
        fieldName.clear();
        fieldName.sendKeys(name);
    }
    public void enterEmail(String email) {
        action.waitElementToBeDisplayed(fieldEmail, 3);
        fieldEmail.clear();
        fieldEmail.sendKeys(email);
    }
    public void enterPhoneNo(String phone) {
        action.waitElementToBeDisplayed(fieldPhoneNo, 3);
        fieldPhoneNo.clear();
        fieldPhoneNo.sendKeys(phone);
    }
    public void enterCity(String city) {
        action.waitElementToBeDisplayed(fieldCity, 3);
        fieldCity.clear();
        fieldCity.sendKeys(city);
    }
    public void enterAddress(String address) {
        action.waitElementToBeDisplayed(fieldAddress, 3);
        fieldAddress.clear();
        fieldAddress.sendKeys(address);
    }
    public void enterPostalCode(String postalcode) {
        action.waitElementToBeDisplayed(fieldPostal, 3);
        fieldPostal.clear();
        fieldPostal.sendKeys(postalcode);
    }
    public void tapButtonCheckout() {
        action.waitElementToBeDisplayed(btnCheckout, 3);
        btnCheckout.click();
    }
    public void enterInfoBuyer(String name, String email, String phone, String city, String address, String postalcode){
        enterName(name);
        enterEmail(email);
        enterPhoneNo(phone);
        enterCity(city);
        enterAddress(address);
        enterPostalCode(postalcode);
        tapButtonCheckout();
    }
}