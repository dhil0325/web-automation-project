package app.binar.test.midtrans;

import app.binar.handler.TestDataProvider;
import app.binar.pages.demoblaze.HomePage;
import app.binar.pages.midtrans.CheckoutPage;
import app.binar.pages.midtrans.Homepage;
import app.binar.pages.midtrans.ShoppingCartPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestBuy {
    WebDriver webDriver;

    @BeforeMethod(alwaysRun = true)
    public void launchChrome() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        webDriver = new ChromeDriver(options);
        webDriver.get("https://demo.midtrans.com");
        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
    }

    @AfterMethod
    public void closeDriver() {
        if (webDriver!=null) {
            webDriver.close();
            System.out.println("Chrome Driver Closed");
        }
    }

    @Test(description = "User Should Be Able To Buy Product")
    public void userShouldBeAbleToBuyProduct() {

        //navigate to Shopping Cart Page
        Homepage homePage = new Homepage(webDriver);
        homePage.tapBtnBuy();

        //get Information
        String name = TestDataProvider.getRandomUserName();
        String email = TestDataProvider.getRandomEmail();
        String phoneNo = TestDataProvider.getRandomPhoneNo();
        String city = TestDataProvider.getRandomCity();
        String address = TestDataProvider.getRandomAddress();
        String postalCode = TestDataProvider.getRandomPostalCode();

        //fill info
        ShoppingCartPage shoppingCartPage = new ShoppingCartPage(webDriver);
        shoppingCartPage.enterInfoBuyer(name, email, phoneNo, city, address, postalCode);





    }

}

