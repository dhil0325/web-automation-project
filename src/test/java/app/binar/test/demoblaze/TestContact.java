package app.binar.test.demoblaze;

import app.binar.handler.Action;
import app.binar.handler.TestDataProvider;
import app.binar.pages.demoblaze.ContactPage;
import app.binar.pages.demoblaze.HomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestContact {
    WebDriver webDriver;

    @BeforeMethod(alwaysRun = true)
    public void launchChrome() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        webDriver = new ChromeDriver(options);
        webDriver.get("https://www.demoblaze.com/");
        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
    }

    @AfterMethod
    public void closeDriver() {
        if (webDriver != null) {
            webDriver.close();
            System.out.println("Chrome Driver Closed");
        }
    }

    @Test(description = "user should be able to send Contact Message")
    public void userShouldBeAbleToSendContactMessage() {
        // navigate to contact page
        HomePage homePage = new HomePage(webDriver);
        homePage.tapNavLinkContact();

        // fill field
        String contactEmail = TestDataProvider.getRandomEmail();
        String contactName = TestDataProvider.getRandomUserName();
        String message = "Halo Selamat Malam";

        // user fill contact message
        ContactPage contactPage = new ContactPage(webDriver);
        contactPage.enterFieldContact(contactEmail, contactName, message);

        //Assertion
        Assert.assertEquals(new Action(webDriver).getMessageAlert(), "Thanks for the message!!");

        // handle alert
        new Action(webDriver).alertHandler(true);



    }
}

