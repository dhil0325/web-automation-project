package app.binar.test.demoblaze;

import app.binar.handler.Action;
import app.binar.handler.TestDataProvider;
import app.binar.pages.demoblaze.HomePage;
import app.binar.pages.demoblaze.LoginPage;
import app.binar.pages.demoblaze.SignUpPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class TestSignUp {
    WebDriver webDriver;

    @BeforeMethod(alwaysRun = true)
    public void launchChrome() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
//      options.setHeadless(true);
        webDriver = new ChromeDriver(options);
        webDriver.get("https://www.demoblaze.com/");
        webDriver.manage().window().fullscreen();
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
    }

    @AfterMethod
    public void closeDriver() {
        if (webDriver != null) {
            webDriver.close();
            System.out.println("Chrome Driver Closed");
        }
    }

    @Test(description = "user should be able to SignUp using valid credential")
    public void userShouldBeAbleToSignUpUsingValidCredential() {
        // navigate to sign up
        HomePage homePage = new HomePage(webDriver);
        homePage.tapNavLinkSignUp();
        // get credential
        String username = TestDataProvider.getRandomUserName();
        String password = TestDataProvider.getRandomUserName();

        // user sign up
        SignUpPage signupPage = new SignUpPage(webDriver);
        signupPage.enterSignup(username, password);
        signupPage.tapSignUp();
        // handle alert
        new Action(webDriver).alertHandler(true);
        // nvaigate to login page
        homePage.tapNavLinkLogin();
        //new user login
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.enterCredential(username, password);
        loginPage.tapLogin();

        // assert login page
        Assert.assertTrue(homePage.getNameOfUser().contains(username));
    }
}
