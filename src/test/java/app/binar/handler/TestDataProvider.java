package app.binar.handler;

import com.github.javafaker.Faker;

public class TestDataProvider {

    private static final Faker faker = new Faker();

    public static String getRandomUserName(){
        return faker.name().firstName();
    }
    public static String getRandomPasswordNumber() {
        return getRandomStringMatchingPattern("[A-Z]\\d{8}");
    }
    public static String getRandomStringMatchingPattern(String pattern) {
        return faker.regexify(pattern);
    }

    public static String getRandomEmail() {
        return getRandomStringMatchingPattern("random\\d{8}") + ".user-binar@test.binar.com";
    }

    public static String getRandomPhoneNo(){
        return faker.phoneNumber().phoneNumber();
    }

    public static String getRandomCity(){
        return faker.address().city();
    }

    public static String getRandomAddress(){
        return faker.address().streetAddress();
    }

    public static String getRandomPostalCode(){
        return faker.address().zipCode();
    }
}


